FROM alpine:3.19

RUN apk add --update npm
RUN npm install --global web-ext

RUN adduser -Ds /bin/bash webext

USER webext
