# wirewall

`wirewall` is a minimal Firefox extension that prevents the user from going down rabbitholes on Reddit. While `wirewall` is active, a user cannot access a reddit url from a reddit page--i.e. they can still google for answers, but can't jump around within reddit.

## Installation

### Installing from Mozilla's Addons Store (Recommended)
1. Click [here](https://addons.mozilla.org/en-US/firefox/addon/wirewall) to open the extension's page
2. Add the extension to your browser

### Installing from Source
1. Go to the [releases tab](https://gitlab.com/Maxwell.Banks/wirewall/-/releases)
2. Click to download the most recent release's `wirewall.zip`
3. Unzip `artifacts.zip` on your local system
4. Go to Firefox's Add-ons Manager
5. Click the gear next **Manage Your Extensions**, then select `Install add-on from file`
6. Navigate to the unzipped directory, then go to `builds/` and select the `.xpi` file inside
7. Install your extension


## Contributing
This is a minimal extension and I plan to keep it that way, but if you have small fixes or suggestions pull requests are welcome!

## Privacy
I don't want your data, I don't care about your data, I don't collect your data.

By definition `wirewall` needs to access your Reddit page data to alter links, but as a quick scan of `wirewall.js` will verify this data is not transmitted anywhere.
