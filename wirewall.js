var anchors = document.getElementsByTagName("a");
const blocklist = ["reddit.com", "/r/", "/u/"]

for (var i = 0; i < anchors.length; i++) {
    if (blocklist.some(item => anchors[i].href.includes(item))) {
        anchors[i].href = "about:blank"
    }
}